package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Scheduler {
    /*
    Schedule is the Sequence of jobs, e.g. for 3 jobs, 3 machines: 002111220 would be a Schedule.
     */
    private final List<List<int[]>> jobs;
    int n_jobs;
    int n_machines;

    List<Integer> lastSchedule = new ArrayList<>();


    public Scheduler(Benchmark benchmark){
        jobs = benchmark.getRepresentation();
        n_jobs = jobs.size();
        n_machines = jobs.get(0).size();
    }



    public List<List<int[]>> getJobs() {
        return jobs;
    }

    List<Integer> fifoSchedule(){

        List<Integer> schedule = new ArrayList<>();
        for (int i = 0; i < n_jobs; i++) {
            for (int j = 0; j < n_machines; j++) {
                schedule.add(i);
            }
        }
        this.lastSchedule =schedule;
        return schedule;
    }

    List<Integer> lifoSchedule(){

        List<Integer> schedule = new ArrayList<>();
        for (int i = 0; i < n_jobs; i++) {
            for (int j = 0; j < n_machines; j++) {
                schedule.add(i);
            }
        }
        Collections.reverse(schedule);
        this.lastSchedule = schedule;
        return schedule;
    }

    List<Integer> randomSchedule(){

        List<Integer> schedule = new ArrayList<>();
        for (int i = 0; i < n_jobs; i++) {
            for (int j = 0; j < n_machines; j++) {
                schedule.add(i);
            }
        }

        Collections.shuffle(schedule);
        this.lastSchedule = schedule;
        return schedule;
    }

    String formatJob(int time, int job_nr){

        String stringrepr = "[%s%s%s]";
        int emptyspace = Math.abs((time-2) / 2);
        String space = "";
        for (int i = 0; i < emptyspace; i++) {
            space+=" ";
        }


        return String.format(stringrepr, space, job_nr,space);
    }

    @Override
    public String toString() {
        String stringRepr ="";
        int [] t_jobs = new int[n_jobs];
        int [] t_machines = new int[n_machines];
        int [] next_op_job = new int[n_jobs];

        String [] output = new String[n_machines];



        for (Integer i : lastSchedule)
        {
            System.out.print(" "+i);
            int [] machine_duration = jobs.get(i).get(next_op_job[i]);
            int machine = machine_duration[0];
            int duration = machine_duration[1];
            next_op_job[i]+=1;
            int start = Math.max(t_jobs[i], t_machines[machine]);
            int leerzeichen = start - t_machines[machine];
            int end = start + duration;
            t_jobs[i] = end;
            t_machines[machine]=end;

            String duration_leerzeichen = "";
            for (int j = 0; j < leerzeichen; j++) {
                duration_leerzeichen+= "-";
            }
            output[machine] += duration_leerzeichen + formatJob(duration,i);
        }
        for (String row: output
             ) {
            stringRepr+=row+"\n";
        }
        stringRepr+= "MakeSpan: " + Arrays.stream(t_machines).max().getAsInt();

        return stringRepr;
    }
}
