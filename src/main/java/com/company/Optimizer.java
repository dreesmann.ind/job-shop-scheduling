package com.company;

import org.javatuples.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Optimizer {
    protected final List<List<int[]>> jobs;
    protected final int n_jobs;
    protected final int n_machines;
    protected final List<Integer> initialSchedule;
    List<List<Integer>> solutions = new ArrayList<>();

    public Optimizer(List<List<int[]>> jobs, List<Integer> initialSchedule) {
        this.jobs = jobs;
        n_jobs = jobs.size();
        n_machines = jobs.get(0).size();
        this.initialSchedule = initialSchedule;
    }

    public int makeSpan(List<Integer> schedule) {
        int[] t_jobs = new int[n_jobs];
        int[] t_machines = new int[n_machines];
        int[] next_op_job = new int[n_jobs];

        for (Integer i : schedule) {

            int[] machine_duration = jobs.get(i).get(next_op_job[i]);
            int machine = machine_duration[0];
            int duration = machine_duration[1];
            next_op_job[i] += 1;
            int start = Math.max(t_jobs[i], t_machines[machine]);
            int end = start + duration;
            t_jobs[i] = end;
            t_machines[machine] = end;
        }
        return Arrays.stream(t_machines).max().getAsInt();
    }

    public abstract Pair<Integer, List<Integer>> optimize(int n_iterations, Integer n_time_seconds);


    protected void printBestSchedule(long time, Pair<Integer, List<Integer>> solution) {
        System.out.println(n_jobs+" Jobs " + n_machines + " Machines\n Best Solution makespan: " + solution.getValue0() + " Took: " + time + " seconds\n Solution: " + solution.getValue1());
    }
}
