package com.company;

import java.util.Arrays;
import java.util.List;

public class MakeSpan {
    final List<Integer> schedule;
    final List<List<int[]>> jobs;
    private final int n_jobs;
    private final int n_machines;

    public MakeSpan(List<Integer> schedule, List<List<int[]>> jobs) {
        this.schedule = schedule;
        this.jobs = jobs;
        n_jobs = jobs.size();
        n_machines = jobs.get(0).size();
    }

    public int calc() {
        int[] t_jobs = new int[n_jobs];
        int[] t_machines = new int[n_machines];
        int[] next_op_job = new int[n_jobs];

        for (Integer i : schedule) {

            int[] machine_duration = jobs.get(i).get(next_op_job[i]);
            int machine = machine_duration[0];
            int duration = machine_duration[1];
            next_op_job[i] += 1;
            int start = Math.max(t_jobs[i], t_machines[machine]);
            int end = start + duration;
            t_jobs[i] = end;
            t_machines[machine] = end;
        }
        return Arrays.stream(t_machines).max().getAsInt();
    }
}
