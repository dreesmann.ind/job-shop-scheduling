package com.company;

public class Operation {
    private int index;
    private int duration;
    private int resource;

    public Operation(int index, int duration, int resource) {
        this.index = index;
        this.duration = duration;
        this.resource = resource;
    }

    public int getResource() {
        return resource;
    }

    public int getDuration() {
        return duration;
    }

    public int getIndex() {
        return index;
    }
}
