package com.company;

import java.util.ArrayList;
import java.util.List;

public class ScheduleNormalizer {

    List<Integer> schedule;
    private final List<List<int[]>> jobs;
    private final int n_jobs;
    private final int n_machines;

    public ScheduleNormalizer(List<Integer> schedule, List<List<int[]>> jobs){
        this.schedule =schedule;
        this.jobs = jobs;
        n_jobs = jobs.size();
        n_machines = jobs.get(0).size();
        normalizeSchedule();
    }

    //Because of the chosen Datastructure, there could be Schedules where not all operations are scheduled,
    //or where more operations are scheduled than exist. Normalization of such schedules happens in this function.
    //Can probably be optimized, but good for now.
    void normalizeSchedule(){
        int [] n_job_occurences = new int[n_jobs];
        List<Integer> normalizedChromosome = new ArrayList<>();
        //Count the occurences of job i in chromosome
        for (Integer i: schedule
             ) {
            if (n_job_occurences[i]<n_machines){
                normalizedChromosome.add(i);
                n_job_occurences[i]+=1;
            }
        }
        // add missing occurences
        for (int i = 0; i < n_job_occurences.length; i++) {
            if (n_job_occurences[i] < n_machines){
                var difference = n_machines-n_job_occurences[i];
                for (int j = 0; j < difference; j++) {
                    normalizedChromosome.add(i);
                }
            }
        }
        this.schedule =normalizedChromosome;
    }
    public List<Integer> schedule(){
        return this.schedule;
    }
}
