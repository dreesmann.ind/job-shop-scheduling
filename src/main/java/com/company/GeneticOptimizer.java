package com.company;

import org.javatuples.Pair;

import java.util.*;

public class GeneticOptimizer extends Optimizer{
    final Scheduler scheduler;
    private Random random;
    public GeneticOptimizer(List<List<int[]>> jobs, List<Integer> initialSchedule, Scheduler scheduler, long seed) {
        super(jobs, initialSchedule);
        this.scheduler = scheduler;
        random = new Random(seed);

    }

    @Override
    public Pair<Integer, List<Integer>> optimize(int n_iterations, Integer n_time_seconds) {
        return null;
    }


    public Pair<Integer, List<Integer>> geneticSearch(int n_generations, long population_size, float mutation_percentage, float selection_percentage){
        List<Pair<Integer,List<Integer>>> solutions = new ArrayList<>();
        long best = 999999999;
        var start_time = System.currentTimeMillis();

        var inititialPopulation = generatePopulations(population_size);
        // fitness is makespan
        var fitness = calcFitness(inititialPopulation);
        var population = mergePopulationAndFitness(inititialPopulation,fitness);

        for (int i = 0; i < n_generations; i++) {
            List<Pair<Integer,List<Integer>>>  the_fittest = select_fittest(population, selection_percentage);

            List<Pair<Integer,List<Integer>>> the_next_generation = new ArrayList<>();

            //crossover
            while (the_next_generation.size() + the_fittest.size() < population_size) {
                var fittest_size = the_fittest.size();
                the_next_generation.add(crossover(the_fittest.get(random.nextInt(fittest_size)).getValue1(), the_fittest.get(random.nextInt(fittest_size)).getValue1()));
            }

            //mutation
            the_next_generation.addAll(the_fittest);
            for (Pair<Integer,List<Integer>> schedule: the_next_generation
                 ) {
                mutate(schedule.getValue1(), mutation_percentage);
            }
            List<List<Integer>> the_schedules = new ArrayList<>();
            for (Pair<Integer,List<Integer>> schedule: the_next_generation
                 ) {
                the_schedules.add(schedule.getValue1());
            }
            var new_sched_fitness = calcFitness(the_schedules);
            var new_population = mergePopulationAndFitness(the_schedules,new_sched_fitness);
            new_population.sort(Comparator.comparing(Pair::getValue0));
            var best_individuum = new_population.get(0);
            if (best_individuum.getValue0() < best){
                best=best_individuum.getValue0();
                solutions.add(best_individuum);
            }


        }

        printBestSchedule((System.currentTimeMillis()-start_time)/1000,solutions.get(solutions.size()-1));
        return solutions.get(solutions.size()-1);

    }

    private void mutate(List<Integer> new_population, float swap_percentage) {
        int swap = (int) Math.abs(swap_percentage*new_population.size());
        for (int i = 0; i < swap; i++) {
            var first = random.nextInt(new_population.size());
            var second = random.nextInt(new_population.size());
            var temp = new_population.get(first);
            new_population.set(first, new_population.get(second));
            new_population.set(second, temp);
        }
    }


    //Crossover with one Cutoff
    private Pair<Integer,List<Integer>> crossover(List<Integer> objects, List<Integer> objects1) {
        var cutoff = random.nextInt(objects.size());

        List<Integer> new_individuum = new ArrayList<>();
        new_individuum.addAll(objects.subList(0,cutoff));
        new_individuum.addAll(objects1.subList(cutoff,objects1.size()));
        var normalized_sched = new ScheduleNormalizer(new_individuum,jobs).schedule();
        return Pair.with(0, normalized_sched);
    }

    private List<Pair<Integer,List<Integer>>> select_fittest(List<Pair<Integer,List<Integer>>> population, float percentage) {
        population.sort(Comparator.comparing(Pair::getValue0));
        return population.subList(0, (int) Math.abs(population.size() * percentage));
    }



    private List<Pair<Integer, List<Integer>>> mergePopulationAndFitness(List<List<Integer>> inititialPopulation, List<Integer> fitness) {
        List<Pair<Integer, List<Integer>>> population = new ArrayList<>();
        for (int i = 0; i < inititialPopulation.size(); i++) {
            population.add(Pair.with(fitness.get(i),inititialPopulation.get(i)));
        }
        return population;
    }

    private List<Integer> calcFitness(List<List<Integer>> population) {
        List<Integer> populationFitness = new ArrayList<>();
        for (List<Integer> individuum:population
             ) {
            var makespan = new MakeSpan(individuum, jobs).calc();
            populationFitness.add(makespan);
        }
        return populationFitness;
    }

    private List<List<Integer>> generatePopulations(long n_populations){
        List<List<Integer>> populations = new ArrayList<>();
        for (int i = 0; i < n_populations; i++) {
            populations.add(scheduler.randomSchedule());
        }
        return populations;
    }

}
