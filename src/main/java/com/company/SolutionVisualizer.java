package com.company;

import java.util.Arrays;
import java.util.List;

public class SolutionVisualizer {
    private final List<Integer> schedule;
    private final List<List<int[]>> jobs;
    int n_jobs;
    int n_machines;

    public SolutionVisualizer(List<Integer> schedule, List<List<int[]>> jobs) {
        this.schedule = schedule;
        this.jobs = jobs;
        n_jobs = jobs.size();
        n_machines = jobs.get(0).size();
    }


    public void visualize() {
        String stringRepr ="";
        int [] t_jobs = new int[n_jobs];
        int [] t_machines = new int[n_machines];
        int [] next_op_job = new int[n_jobs];

        String [] output = new String[n_machines];



        for (Integer i : schedule)
        {
            System.out.print(" "+i);
            int [] machine_duration = jobs.get(i).get(next_op_job[i]);
            int machine = machine_duration[0];
            int duration = machine_duration[1];
            next_op_job[i]+=1;
            int start = Math.max(t_jobs[i], t_machines[machine]);
            int leerzeichen = start - t_machines[machine];
            int end = start + duration;
            t_jobs[i] = end;
            t_machines[machine]=end;

            String duration_leerzeichen = "";
            for (int j = 0; j < leerzeichen; j++) {
                duration_leerzeichen+= "-";
            }
            output[machine] += duration_leerzeichen + formatJob(duration,i);
        }
        for (String row: output
        ) {
            stringRepr+=row+"\n";
        }
        stringRepr+= "MakeSpan: " + Arrays.stream(t_machines).max().getAsInt();

        System.out.println(stringRepr);
    }

    private String formatJob(int time, int job_nr){

        String stringrepr = "[%s%s%s]";
        int emptyspace = Math.abs((time-2) / 2);
        String space = "";
        for (int i = 0; i < emptyspace; i++) {
            space+=" ";
        }


        return String.format(stringrepr, space, job_nr,space);
    }
}
