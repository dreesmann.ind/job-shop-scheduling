package com.company;

public class Resource {
    private int id;

    public int getId() {
        return id;
    }

    public boolean equals(Resource resource) {
        return resource.getId() == getId();
    }
}
